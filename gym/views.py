from django.contrib.auth.forms import UserChangeForm
from django.shortcuts import  render, redirect
from .forms import NewUserForm, MyAuthForm
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages
from .models import Trainer, Classe
from django.views.generic import ListView
from datetime import datetime
from django.contrib.auth.decorators import login_required


def login_user(request):
	if request.method == "POST":
		form = MyAuthForm(request, data=request.POST)
		if form.is_valid():
			username = form.cleaned_data.get('username')
			password = form.cleaned_data.get('password')
			user = authenticate(username=username, password=password)
			if user is not None:
				login(request, user)
				# messages.info(request, f"Prisijungimas sėkmingas!")
				return redirect("home")
			else:
				messages.info(request,"Netaisyklingas vartotojo vardas arba slaptažodis.")
		else:
			messages.info(request,"Netaisyklingas vartotojo vardas arba slaptažodis.")
	form = MyAuthForm()
	return render(request=request, template_name="login.html", context={"login_form":form})


def register_user(request):
	if request.method == "POST":
		form = NewUserForm(request.POST)
		if form.is_valid():
			user = form.save()
			# login(request, user)
			messages.success(request, "Registracija sėkminga.")
			return redirect("login")
		messages.error(request, "Nesėkminga reigstracija, klaidingi duomenys.")
	form = NewUserForm()
	return render(request=request, template_name="register.html", context={"register_form":form})

def logout_user(request):
	logout(request)
	messages.info(request, "Atsijungėtė sėkmingai.")
	return redirect("login")


class TrainerListView(ListView):
    model = Trainer
    template_name = 'home.html'
    context_object_name = 'Trainer'
    ordering = ['-id']


@login_required(login_url='/')
def trainer_classes(request, trainer_id):
	trainer_clases_data = []
	trainer_classes = Classe.objects.filter(trainer=trainer_id)
	trainer = Trainer.objects.get(id=trainer_id)
	
	for trainer_class in trainer_classes:
		clients = []
		client_number = 0
		current_user = request.user
		full_class = False

		for client in trainer_class.clients.prefetch_related():
			clients.append({'client_name': client})
			client_number += 1
			if current_user == client:
				full_class = True

		

		if client_number == 5 or client_number > 5:
			full_class = True

		converted_date = trainer_class.date.strftime("%Y-%m-%d %H:%M")
		present = datetime.now()
		date_to_check = datetime.strptime(converted_date, "%Y-%m-%d %H:%M")

		if not date_to_check.date() < present.date():
			trainer_clases_data.append({
				'id': trainer_class.id,
				'title': trainer_class.title,
				'date': converted_date,
				'clients': clients,
				'time': trainer_class.time,
				'client_number': client_number,
				'full_class': full_class
				})
	
	trainer_clases_data.sort(key=lambda x: datetime.strptime(x['date'], '%Y-%m-%d %H:%M'))
	return render(request,'trainer_classes.html',{'classes': trainer_clases_data, 'trainer': trainer})


@login_required(login_url='/')
def register_to_class(request, class_id):
	trainer_class = Classe.objects.get(id=class_id)
	class_data = {'title': trainer_class.title, 'date': trainer_class.date.strftime("%Y-%m-%d %H:%M"), 'trainer': trainer_class.trainer.id, 'trainer_name': trainer_class.trainer, 'id': trainer_class.id}

	return render(request, 'register_to_class.html', {'class': class_data})


@login_required(login_url='/')
def register_approve(request, class_id):
	current_user = request.user
	trainer_class = Classe.objects.get(id=class_id)
	trainer_class.clients.add(current_user)
	messages.info(request,f"Užsiregistravo sėkmingai! ({trainer_class.title} - {trainer_class.trainer})")
	return redirect("home")


@login_required(login_url='/')
def profile(request):
	curent_user = request.user
	user_classes = Classe.objects.filter(clients=curent_user.id)
	user_classes_formatted = []

	for user_class in user_classes:
		user_classes_formatted.append({'title': user_class.title, 'date': user_class.date.strftime("%Y-%m-%d %H:%M"), 'trainer': user_class.trainer})

	user_classes_formatted.sort(key=lambda x: datetime.strptime(x['date'], '%Y-%m-%d %H:%M'))
	return render(request, 'profile.html', {'classes': user_classes_formatted, 'user': curent_user})