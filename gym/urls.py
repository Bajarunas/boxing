from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from .views import register_user, login_user, logout_user, TrainerListView, trainer_classes, register_to_class, register_approve, profile


urlpatterns = [
    path('', login_user, name='login'),
    path('register', register_user, name='register'),
    path('logout', logout_user, name= "logout"),
    path('home', TrainerListView.as_view(), name='home'),
    path('trainer/<trainer_id>', trainer_classes, name='trainer_classes'),
    path('trainer/class/<class_id>', register_to_class, name='register_to_class'),
    path('register_approve/<class_id>', register_approve, name='regiter_approve'),
    path('profile', profile, name='profile')
    # path('home', home, name='home'),
    # path('contact/', contact, name='contact'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
