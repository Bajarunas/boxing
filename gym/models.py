from django.db import models
from datetime import datetime
from django.contrib.auth.models import User


class Course(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title

class Trainer(models.Model):
    full_name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    trainer_image = models.ImageField(upload_to ='gym/static/trainers')

    def __str__(self):
        return self.full_name

class Classe(models.Model):
    title = models.CharField(max_length=100)
    date = models.DateTimeField(default=datetime.now, blank=True)
    clients = models.ManyToManyField(User, blank=True)
    trainer = models.ForeignKey(Trainer, related_name='trainer_related', on_delete=models.CASCADE)
    time = models.FloatField(default=0)

    def __str__(self):
        return self.title