from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User


# Create your forms here.
class MyAuthForm(AuthenticationForm):
    class Meta:
        model = User
        fields = ['username', 'password']

    def __init__(self, *args, **kwargs):
        super(MyAuthForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget = forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Vartotojo vardas'})
        self.fields['username'].label = False
        self.fields['password'].widget = forms.PasswordInput(
            attrs={'class': 'form-control', 'placeholder': 'Slaptažodis'})
        self.fields['password'].label = False


class NewUserForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update(
            {'class': 'form-control', 'placeholder': ('Vartotojo vardas')})
        self.fields['email'].widget.attrs.update(
            {'class': 'form-control', 'placeholder': ('El paštas')})
        self.fields['password1'].widget.attrs.update(
            {'class': 'form-control', 'placeholder': ('Slaptažodis')})
        self.fields['password2'].widget.attrs.update(
            {'class': 'form-control', 'placeholder': ('Pakartokite')})
